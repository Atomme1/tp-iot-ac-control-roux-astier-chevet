# aws_lambda_function to control air conditioner
resource "aws_lambda_function" "lambda_air_control" {
  filename      = "files/empty_package.zip"
  function_name = "ac_control_lambda"
  role          = aws_iam_role.lambda_role.arn
  handler       = "ac_control_lambda.lambda_handler"
  runtime       = "python3.7"
}
# aws_cloudwatch_event_rule scheduled action every minute
resource "aws_cloudwatch_event_rule" "cloudwatch_rule_check_minute" {
    name        = "cloudwatch_capture_data"
    schedule_expression = "rate(1 minute)"
}

# aws_cloudwatch_event_target to link the schedule event and the lambda function
resource "aws_cloudwatch_event_target" "lambda" {
    target_id = "lambda"
    rule      = aws_cloudwatch_event_rule.cloudwatch_rule_check_minute.name
    arn       = aws_lambda_function.lambda_air_control.arn
}

# aws_lambda_permission to allow CloudWatch (event) to call the lambda function
resource "aws_lambda_permission" "lambda_permission_cloudwatch" {
    statement_id  = "AllowExecutionFromCloudWatch"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.lambda_air_control.function_name
    principal     = "events.amazonaws.com"
    source_arn    = aws_cloudwatch_event_rule.cloudwatch_rule_check_minute.arn

}
# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "timestream_database" {
    database_name = "iot"
}
# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "timestream_table" {
    database_name = aws_timestreamwrite_database.timestream_database.database_name
    table_name    = "temperaturesensor"
}